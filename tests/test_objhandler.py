#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 15:57:43 2021

@author: lcalmett
"""

from unittest import TestCase
import unittest
import numpy as np
import filecmp
import os
from smithers.io import OBJHandler

obj_file = "init.obj"


class TestOBJHandler(TestCase):
    def test_points(self):
        data = OBJHandler.read(obj_file)
        np.testing.assert_array_almost_equal(data['points'][0], [-0.5] * 3)
    
    def test_number_points(self):
        data = OBJHandler.read(obj_file)
        np.testing.assert_equal(data['points'].shape, (8, 3))

    def test_cells(self):
        data = OBJHandler.read(obj_file)
        np.testing.assert_equal(data['cells'][5], [6, 1, 4])

    def test_write(self):
        data = OBJHandler.read(obj_file)
        data['points'] += 1.0
        OBJHandler.write('test.obj', data)

a=TestOBJHandler()

b=a.test_write()